<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- <script src="https://cdn.tailwindcss.com"></script> -->
  <link rel="stylesheet" href="style/output.css">
  <link rel="stylesheet" href="style/sweetalert2.min.css">
  <script type="text/javascript" src="style/sweetalert2.all.min.js"></script>
</head>
<body>
    <div class="container mx-auto px-4">
        <!-- Navigation bar -->
        <nav class="shadow-lg">
            <div class="flex px-5 py-5">
                <div class="mr-2">
                    <a href="<?php echo URL_HELPER::createLink('user/user_controller','index','')?>" class="text-xl">Si-Buk</a>
                </div>
                <div class="mr-2">
                    <a href="<?php echo URL_HELPER::createLink('user/user_controller','index','')?>" class="">User</a>
                </div>
                <div class="mr-2">
                    <a href="<?php echo URL_HELPER::createLink('buku/buku_controller','index','')?>" class="">Buku</a>
                </div>
            </div>
        </nav>
