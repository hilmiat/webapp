<?php
if(defined('BASE_PATH')){
    require_once BASE_PATH.'/user/model_user.php';
}else{
    require_once '../koneksi.php';
    require_once '../user/model_user.php';
}

class Buku{
    public $id;
    public $ISBN;
    public $judul;
    public $id_penerbit;
    public $id_penulis;
    //obj user sebagai penulis
    public $user;//NIK,nama,alamat
    public $is_available;


    public function __construct($id, $ISBN, $judul, $id_penerbit,$id_penulis,$user,$is_available){
        $this->id = $id;
        $this->ISBN = $ISBN;
        $this->judul = $judul;
        $this->id_penerbit = $id_penerbit;
        $this->id_penulis = $id_penulis;
        $this->user = $user;
        $this->is_available = $is_available;
    }
}


class ModelBuku{
    private $conn;
    //method untuk membuat obj koneksi
    private function getConnection(){
        //kalau sebelumnya belum ada obj koneksi, maka buat
        if($this->conn==null){
            $con = new Connection();
            $this->conn = $con->getConnection();
        }
    }

    //method untuk get all 
    public function getAllBuku($criteria=array('searchValue'=>'','sort'=>'','limit'=>'10','offset'=>'0')){

        $searchQuery = "WHERE buku.ISBN LIKE '%".$criteria['searchValue']."%' OR buku.judul LIKE '%".$criteria['searchValue']."%' OR user.nama LIKE '%".$criteria['searchValue']."%'";
        
        $orderBy = $criteria['sort']==""?"":"ORDER BY ".$criteria['sort'];

        $limit = ' LIMIT '.$criteria['limit'];
        $offset = ' OFFSET '.$criteria['offset'];
        //call connection and fetch data
        $this->getConnection();
        //buat query untuk select all
        $sql = "SELECT * FROM buku INNER JOIN user on buku.id_penulis = user.id $searchQuery $orderBy $limit $offset;";
        // prepare statement
        $stmt = $this->conn->prepare($sql);
        //execute statement
        $stmt->execute();
        //fetch data
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //create array of buku
        $bukus = array();
        foreach($result as $r){
            //buat buat user -> sebagai penulis
            $user = new User($r['id'],$r['NIK'],$r['nama'],$r['id_propinsi'],$r['alamat']);
            
            //buat object buku untuk tiap row data
            $buku = new Buku($r['id'],$r['ISBN'],$r['judul'],$r['id_penerbit'],$r['id_penulis'],$user,$r['is_available']);
            //simpan dalam array of buku
            $bukus[] = $buku;
        }
        return $bukus;
    }
    //method untuk insert user
    public function insertUser($user_baru){
        //buat obj koneksi
        $this->getConnection();
        //sql
        $sql = "INSERT INTO user(NIK, nama, alamat) values(:NIK, :nama, :alamat)";
        //prepared statement
        $stmt =  $this->conn->prepare($sql);
        //bind param
        $stmt->bindParam(':NIK',$user_baru->NIK);
        $stmt->bindParam(':nama',$user_baru->nama);
        $stmt->bindParam(':alamat',$user_baru->alamat);
        //eksekusi query
        $stmt->execute();
    }

    //method untuk update user
    public function updateUser($id,$user_baru){
        //buat obj koneksi
        $this->getConnection();
        //sql
        $sql = "UPDATE user SET NIK=:NIK, nama=:nama, alamat=:alamat WHERE id=:id";
        //prepared statement
        $stmt =  $this->conn->prepare($sql);
        //bind param
        $stmt->bindParam(':NIK',$user_baru->NIK);
        $stmt->bindParam(':nama',$user_baru->nama);
        $stmt->bindParam(':alamat',$user_baru->alamat);
        $stmt->bindParam(':id',$id);
        //eksekusi query
        $stmt->execute();
    }

    //method untuk delete user
    public function deleteUser($id_dihapus){
        //buat obj koneksi
        $conn = new Connection();
        //siapkan query
        $sql = "DELETE FROM user WHERE id=:id";
        try{
            //prepare statement
            $stmt = $conn->getConnection()->prepare($sql);
            //bind parameter
            $stmt->bindParam(':id',$id_dihapus);
            //eksekusi
            $stmt->execute();
            return true;
        }catch(Exception $e){
            echo 'gagal hapus data';
            return false;
        }
       
    }

    //method untuk get user by id
    public function getUserById($id){
         //call connection and fetch data
         $this->getConnection();
         //buat query untuk select all
         $sql = "SELECT * FROM user WHERE id=:id";
         // prepare statement
         $stmt = $this->conn->prepare($sql);
         //bind param
         $stmt->bindParam(':id',$id);
         //execute statement
         $stmt->execute();
         //fetch data
         $result = $stmt->fetch(PDO::FETCH_ASSOC);
         //cek, apakah ada user dengan id yang dimaksud
         if(isset($result['id'])){
             //create obj user
             $user = new User($result['id'],$result['NIK'],$result['nama'],$result['alamat']);
             return $user;
         }else{
            return null;
         }
    }
    public function getTotalBuku(){
        $this->getConnection();
        $sql = "SELECT COUNT(*) as total FROM buku";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result['total'];
    }
}