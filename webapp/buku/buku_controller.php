<?php

if(defined('BASE_PATH')){
    require_once BASE_PATH.'/buku/model_buku.php';
}else{
    require_once 'model_buku.php';
}
class BukuController{
    private $model;
    public function __construct(){
        // buat obj model buku
        $this->model = new ModelBuku(); 
    }
    public function action(){
        $act = isset($_GET['act'])?$_GET['act']:'index';
        //routing modul user
        // $act = $_GET['act']??'index';
        switch($act){
            case 'index':
                $this->index();
                break;
            case 'add':
                $this->create();
                break;
            case 'delete':
                $this->delete();
                break;
            case 'update':
                $this->update();
                break;
            case 'ajax':
                $this->ajax();
                break;
            default:
                $this->index();
        }
    }
    //action index
    public function index(){
        //get all data buku
        $bukus = $this->model->getAllBuku();
        //tampilkan data buku dlm bentuk tabel
        // include '../template/header.php';
        include 'view/tabel_buku.php';
        // include '../template/footer.php';
    }
    //action delete
    public function delete(){
        //baca id yang mau di delete
        $id_delete = $_GET['id'];
        $status = $this->model->deleteUser($id_delete);
        if($status){
            header('location:user_controller.php');
        }
    }
    //action add
    public function create(){
        //cek apakah menampilkan form atau proses form
        if(isset($_POST['submit'])){
            //proses data
            $user_baru = new User('',$_POST['NIK'],$_POST['nama'],$_POST['alamat']);
            //panggil fungsi insertUser
            $this->model->insertUser($user_baru);
            header('location:user_controller.php');
        }else{
            // $this->loadView('view/form_user');
            include 'view/form_user.php';
        }
    }
    //action update
    public function update(){
        //baca parameter id user yang akan diupdate
        $id_diupdate = $_GET['id'];
        //get user berdasarkan id
        $user = $this->model->getUserById($id_diupdate);
        if($user==null){
            echo 'user not found';
        }else{
             //cek apakah menampilkan form atau proses form
            if(isset($_POST['submit'])){
                //proses data
                $user_baru = new User('',$_POST['NIK'],$_POST['nama'],$_POST['alamat']);
                //panggil fungsi updateuser
                $this->model->updateUser($id_diupdate,$user_baru);
                header('location:user_controller.php');
            }else{
                // $this->loadView('view/form_update',array('user'=>$user));
                include 'view/form_update.php';
            }
        }

    }

    //fungsi untuk load tampilan
    private function loadView($file,$data){
        include $file.'.php';
    }

    //fungsi untuk menerima request dari datatable
    public function ajax(){
       //baca request dari datatable
       //search value
        $search = $_GET['search']['value'];
        // echo 'search value: '.$search;
        $criteria = array(
            'searchValue'=>$search
        );
        //sorting
        //nama column
        $columns = $_GET['columns'];
        //sorting column
        $order = $_GET['order'];
        //column yang di sorting
        $orderColumn = $columns[$order[0]['column']]['data'];
        //sorting direction
        $orderDir = $order[0]['dir'];
        $criteria['sort'] = $orderColumn.' '.$orderDir;

        //paging
        $start = $_GET['start'];
        $length = $_GET['length'];
        $criteria['limit'] = $length;
        $criteria['offset'] = $start;

        $bukus = $this->model->getAllBuku($criteria);
        //mendapatkan total record tanpa filter atau limit
        $total = $this->model->getTotalBuku();
        echo json_encode(
            array(
                'draw'=>$_GET['draw'],
                'recordsTotal'=>$total,
                'recordsFiltered'=>($search=='')?$total:count($bukus),
                'data'=>$bukus
            )
        );
    }
}
$bukuController = new BukuController();
$bukuController->action();
?>