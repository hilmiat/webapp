<head>
    <link rel="stylesheet" type="text/css" href="buku/view/test.css"/>
    <link rel="stylesheet" type="text/css" href="style/output.css"/>
    <link rel="stylesheet" type="text/css" href="buku/datatable/datatables.min.css"/>
    <script type="text/javascript" src="buku/datatable/datatables.editor.min.js"></script>
    <script type="text/javascript" src="buku/datatable/datatables.min.js"></script>
</head>
<body>
<h1 class="merah">Data Buku</h1>
<p>Test</p>
<p class="merah">Test 2</p>
<a href="buku_controller.php?act=add">Buat buku</a>
<table id="tabelKu" class="min-w-full text-left text-sm font-light" >
    <thead>
        <tr>
            <th>ISBN</th>
            <th>Judul</th>
            <th>Penerbit</th>
            <th>id Penulis</th>
            <th>Penulis</th>
            <th>Status</th>
            <!-- <th>Aksi</th> -->
        </tr>
    </thead>
    <tbody>
        <?php
            foreach($bukus as $u){
        ?>
                <tr>

                    <td><?php echo $u->ISBN;?></td>
                    <td><?php echo $u->judul;?></td>
                    <td><?php echo $u->id_penerbit;?></td>
                    <td><?php echo $u->id_penulis.'-'.$u->user->id;?></td>
                    <td><?php echo $u->user->nama;?></td>
                    <td><?php echo $u->is_available;?></td>
                    <!-- <td>
                        <a href="buku_controller.php?act=update&id=<?php echo $u->id;?>">Edit</a>
                        <a href="buku_controller.php?act=delete&id=<?php echo $u->id;?>">Hapus</a>
                    </td> -->
                </tr>
            
        <?php
            }
        ?>
    </tbody>
</table>

<form id="FormBuku">
    <input type="text" name="ISBN" placeholder="ISBN">
    <input type="text" name="judul" placeholder="Judul">
    <input type="submit" name="submit" value="Submit">
</form>

</body>

<script>
    // let table = new DataTable('#tabelKu');
    // table.serverSide=true;
    // table.ajaxUrl='buku_controller.php?act=ajax';

var editor;
    $(document).ready(function() {

        $('#tabelKu').DataTable({
            serverSide:true,
            ajax:'<?php 
                echo URL_HELPER::createLink('buku/buku_controller','ajax',array('ajax'=>true))?>',
            columns:[
                {data:'ISBN'},
                {data:'judul'},
                {data:'id_penerbit'},
                {data:'id_penulis'},
                {data:'user.nama'},
                {data:'is_available'},
                // {data:'aksi'}
            ],
            // buttons: [
            //     { extend: "create", editor: editor },
            //     { extend: "edit",   editor: editor },
            //     { extend: "remove", editor: editor }
            // ]
        });
    } );
</script>