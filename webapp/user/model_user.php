<?php
// require_once "../koneksi.php";
if(!defined('BASE_PATH')){
    require_once '../koneksi.php';
}
class User{
    public $id;
    public $NIK;
    public $nama;
    public $id_propinsi;
    public $alamat;
    public function __construct($id, $NIK, $nama, $id_propinsi,$alamat){
        $this->id = $id;
        $this->NIK = $NIK;
        $this->nama = $nama;
        $this->id_propinsi = $id_propinsi;
        $this->alamat = $alamat;
    }
    // public function __construct(){
    //     //do nothing
    // }
}

// $user1 = new User("1","123456789","Rizki","Jl. Cikutra");
// $user2 = new User();

class ModelUser{
    private $conn;
    //method untuk membuat obj koneksi
    private function getConnection(){
        //kalau sebelumnya belum ada obj koneksi, maka buat
        if($this->conn==null){
            $con = new Connection();
            $this->conn = $con->getConnection();
        }
    }

    //method untuk get all user
    public function getAllUser(){
        //call connection and fetch data
        $this->getConnection();
        //buat query untuk select all
        $sql = "SELECT * FROM user";
        // prepare statement
        $stmt = $this->conn->prepare($sql);
        //execute statement
        $stmt->execute();
        //fetch data
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // //get column names
        // $columns = array();
        // foreach($result[0] as $kolom=>$r){
        //     $columns[] = $kolom;
        // }
        // print_r($columns);

        //create array of user
        $users = array();
        foreach($result as $r){
            //buat object user untuk tiap row data
            $user = new User($r['id'],$r['NIK'],$r['nama'],$r['id_propinsi'],$r['alamat']);
            //simpan dalam array of user
            $users[] = $user;
        }
        return $users;
    }
    //method untuk insert user
    public function insertUser($user_baru){
        //buat obj koneksi
        $this->getConnection();
        //sql
        $sql = "INSERT INTO user(NIK, nama, id_propinsi, alamat) values(:NIK, :nama,:id_propinsi, :alamat)";
        //prepared statement
        $stmt =  $this->conn->prepare($sql);
        //bind param
        $stmt->bindParam(':NIK',$user_baru->NIK);
        $stmt->bindParam(':nama',$user_baru->nama);
        $stmt->bindParam(':id_propinsi',$user_baru->id_propinsi);
        $stmt->bindParam(':alamat',$user_baru->alamat);
        //eksekusi query
        $stmt->execute();
    }

    //method untuk update user
    public function updateUser($id,$user_baru){
        //buat obj koneksi
        $this->getConnection();
        //sql
        $sql = "UPDATE user SET NIK=:NIK, nama=:nama, id_propinsi=:id_propinsi, alamat=:alamat WHERE id=:id";
        //prepared statement
        $stmt =  $this->conn->prepare($sql);
        //bind param
        $stmt->bindParam(':NIK',$user_baru->NIK);
        $stmt->bindParam(':nama',$user_baru->nama);
        $stmt->bindParam(':id_propinsi',$user_baru->id_propinsi);
        $stmt->bindParam(':alamat',$user_baru->alamat);
        $stmt->bindParam(':id',$id);
        //eksekusi query
        $stmt->execute();
    }

    //method untuk delete user
    public function deleteUser($id_dihapus){
        //buat obj koneksi
        $conn = new Connection();
        //siapkan query
        $sql = "DELETE FROM user WHERE id=:id";
        try{
            //prepare statement
            $stmt = $conn->getConnection()->prepare($sql);
            //bind parameter
            $stmt->bindParam(':id',$id_dihapus);
            //eksekusi
            $stmt->execute();
            return true;
        }catch(Exception $e){
            echo 'gagal hapus data';
            return false;
        }
       
    }

    //method untuk get user by id
    public function getUserById($id){
         //call connection and fetch data
         $this->getConnection();
         //buat query untuk select all
         $sql = "SELECT * FROM user WHERE id=:id";
         // prepare statement
         $stmt = $this->conn->prepare($sql);
         //bind param
         $stmt->bindParam(':id',$id);
         //execute statement
         $stmt->execute();
         //fetch data
         $result = $stmt->fetch(PDO::FETCH_ASSOC);
         //cek, apakah ada user dengan id yang dimaksud
         if(isset($result['id'])){
             //create obj user
             $user = new User($result['id'],$result['NIK'],$result['nama'],$result['id_propinsi'],$result['alamat']);
             return $user;
         }else{
            return null;
         }
    }

    //method untuk mencari user dengan kriteria tertentu
    public function findUser($criteria){
        //criteria berupa array contoh: array('field'=>'NIK','searchvalue'=>'3434')
        //jika filed berupa array contoh: array('field'=>array('NIK','nama'),'searchvalue'=>'3434')
        $searchQuery = "";
        if(is_array($criteria['field'])){
            $fields = $criteria['field'];
            for($i=0;$i<count($fields);$i++){
                $searchQuery .= $fields[$i]." LIKE :searchvalue ";
                if($i<count($fields)-1){
                    $searchQuery .= 'OR ';
                }
            }
            // foreach($fields as $f){
            //     $searchQuery .= $f." LIKE :searchvalue OR ";
            // }
        }else{
            $field = $criteria['field'];
            $searchQuery = $field." LIKE :searchvalue ";
        }
        $searchvalue = $criteria['searchvalue'];
        //call connection and fetch data
        $this->getConnection();
        //buat query untuk select all
        // $sql = "SELECT * FROM user WHERE $field like :searchvalue ";
        $sql = "SELECT * FROM user WHERE $searchQuery";
        
        //jika ada parameter sort
        // array('field'=>array('NIK','nama'),'searchvalue'=>'3434','sort'=>'nama DESC')
        if(isset($criteria['sort'])){
            $sql .= 'ORDER BY '.$criteria['sort'];
        }
        //baca criteria limit, offset
        // if(isset($criteria['limit']) && isset($criteria['offset'])){
        //     $sql .= ' limit '.$criteria['limit'].' offset '.$criteria['offset'];
        // }

        // prepare statement
        $stmt = $this->conn->prepare($sql);
        //bind param
        $stmt->bindParam(':searchvalue',$searchvalue);
        //execute statement
        $stmt->execute();
        //fetch data
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //create array of user
        $users = array();
        foreach($result as $r){
            //buat object user untuk tiap row data
            $user = new User($r['id'],$r['NIK'],$r['nama'],$r['id_propinsi'],$r['alamat']);
            //simpan dalam array of user
            $users[] = $user;
        }
        return $users;
   }
}