

<a class="text-sky-500
        transition duration-150 ease-in-out 
        hover:text-sky-600 focus:text-sky-600 
        active:text-sky-700" 
    href="<?php echo URL_Helper::createLink('user/user_controller','add',null);?>"
>Buat User</a>
<form>
    <div class="flex justify-center flex-col items-center">
        <div class="mb-3 xl:w-96">
            <div class="relative mb-4 flex w-full flex-wrap items-stretch">
            <input
                name="paramCari"
                type="search"
                class="relative m-0 -mr-px block w-[1%] min-w-0 flex-auto rounded-l border border-solid border-neutral-300 bg-transparent bg-clip-padding px-3 py-1.5 text-base font-normal text-neutral-700 outline-none transition duration-300 ease-in-out focus:border-sky focus:text-neutral-700 focus:shadow-te-sky focus:outline-none dark:text-neutral-200 dark:placeholder:text-neutral-200"
                placeholder="Search"
                aria-label="Search"
                aria-describedby="button-addon1" 
                />
            <button
                class="relative z-[2] flex items-center rounded-r bg-sky-500 px-6 py-2.5 text-xs font-medium uppercase leading-tight text-white shadow-md transition duration-150 ease-in-out hover:bg-sky-700 hover:shadow-lg focus:bg-sky-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-sky-800 active:shadow-lg"
                type="submit"
                id="button-addon1"
                data-te-ripple-init
                data-te-ripple-color="light">
                <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                class="h-5 w-5">
                <path
                    fill-rule="evenodd"
                    d="M9 3.5a5.5 5.5 0 100 11 5.5 5.5 0 000-11zM2 9a7 7 0 1112.452 4.391l3.328 3.329a.75.75 0 11-1.06 1.06l-3.329-3.328A7 7 0 012 9z"
                    clip-rule="evenodd" />
                </svg>
            </button>
            </div>
        </div>
        <?php
            if($paramCari!=""){
        ?>
            <p>Menampilkan hasil pencarian untuk <b>"<?php echo $paramCari?>"</b></p>
            <button
                class="relative z-[2] flex items-center rounded bg-sky-500 px-6 py-2.5 text-xs font-medium uppercase leading-tight text-white shadow-md transition duration-150 ease-in-out hover:bg-sky-700 hover:shadow-lg focus:bg-sky-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-sky-800 active:shadow-lg"
                type="submit"
                id="button-addon1"
                data-te-ripple-init
                data-te-ripple-color="light">
                Clear
            </button>
        <?php
            }
        ?>
    </div>
</form>


<table class="min-w-full text-left text-sm font-light" id="myTable">
    <thead class="border-b font-medium dark:border-neutral-500">
        <form>
        <tr>
            <th scope="col" class="px-6 py-4">NIK <button type="submit" name="sortParam" value="NIK ASC">desc</button></th>
            <th scope="col" class="px-6 py-4">Nama <button type="submit" name="sortParam" value="nama DESC">desc</button></th>
            <th scope="col" class="px-6 py-4">Propinsi <button type="submit" name="sortParam" value="nama DESC">desc</button></th>
            <th scope="col" class="px-6 py-4">Alamat <button type="submit" name="sortParam" value="alamat DESC">desc</button></th>
            <th scope="col" class="px-6 py-4">Aksi</th>
        </tr>
        </form>
    </thead>
    <tbody>
        <?php
            foreach($users as $u){
        ?>
                <tr class="border-b dark:border-neutral-500">

                    <td class="whitespace-nowrap px-6 py-4 font-medium"><?php echo $u->NIK;?></td>
                    <td class="whitespace-nowrap px-6 py-4"><?php echo $u->nama;?></td>
                    <td class="whitespace-nowrap px-6 py-4"><?php echo $u->id_propinsi;?></td>
                    <td class="whitespace-nowrap px-6 py-4"><?php echo $u->alamat;?></td>
                    <td class="whitespace-nowrap px-6 py-4">
                        <a 
                            class="text-amber-500
                                    transition duration-150 ease-in-out 
                                    hover:text-amber-600 focus:text-amber-600 
                                    active:text-amber-700"                 
                            href="<?php echo URL_HELPER::createLink('user/user_controller','update',array('id'=>$u->id)) ?>">
                                Edit
                        </a>
                        <a 
                            id="hapus"
                            onclick="bukaPopup(this)"
                            class="text-red-500
                                    transition duration-150 ease-in-out 
                                    hover:text-red-600 focus:text-red-600 
                                    active:text-red-700"
                            href="<?php echo URL_HELPER::createLink('user/user_controller','delete',array('id'=>$u->id)) ?> ?>" >Hapus</a>
                    </td>
                </tr>
            
        <?php
            }
        ?>
    </tbody>
</table>
<div class="mt-4">
<nav aria-label="Page navigation example">
    <form>
    <ul class="inline-flex -space-x-px">
        <li>
            <a href="#page=1"
                class="bg-white border border-gray-300 text-gray-500 hover:bg-gray-100 hover:text-gray-700 ml-0 rounded-l-lg leading-tight py-2 px-3 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">Previous</a>
        </li>
        <?php 
            for($i=1;$i<5;$i++){
        ?>
        <li>
            <button type="submit" value="<?php echo $i;?>" name="page"
                class="bg-white border border-gray-300 text-gray-500 hover:bg-gray-100 hover:text-gray-700 leading-tight py-2 px-3 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"><?php echo $i;?></button>
        </li>
        <?php
            }
        ?>
        <li>
            <a href="#"
                class="bg-white border border-gray-300 text-gray-500 hover:bg-gray-100 hover:text-gray-700 rounded-r-lg leading-tight py-2 px-3 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">Next</a>
        </li>
    </ul>
    </form>
</nav>
</div>
<script>

    function bukaPopup(obj){
            Swal.fire({
                title: 'Apakah yakin ingin hapus data?',
                showDenyButton: true,
                confirmButtonText: 'Hapus',
                denyButtonText: 'Tidak',
            }).then(result=>{
                if(result.isConfirmed){
                    Swal.fire('Sukses Hapus!', '', 'success')
                    window.location = obj.href;
                }else if(result.isDenied){
                    Swal.fire('Batal hapus', '', 'info')
                }
            });
            event.preventDefault();
    }

</script>