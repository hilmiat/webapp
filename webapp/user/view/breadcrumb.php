<nav class="w-full rounded-md p-4">
  <ol class="list-reset flex">
    <li>
      <a
        href="index.php?mod=<?php echo $linkModule;?>"
        class="text-sky-500 transition duration-150 ease-in-out hover:text-sky-600 focus:text-sky-600 active:text-sky-700 dark:text-sky-400 dark:hover:text-sky-500 dark:focus:text-sky-500 dark:active:text-sky-600"
        ><?php echo $namaModule;?></a
      >
    </li>
    <li>
      <span class="mx-2 text-neutral-500 dark:text-neutral-400">/</span>
    </li>
    <li class="text-neutral-500 dark:text-neutral-400"><?php echo $halaman;?></li>
  </ol>
</nav>