<?php
    $ar_prop = array(
        '0'=>'---Pilih Propinsi---',
        '1'=>'Jakarta',
        '2'=>'Jawa Barat'
    );

?>


<form method="POST">
    <div class="flex justify-center flex-col">
        <!-- NIK -->
        <div class="mb-3 w-96">
            <label
            for="NIK"
            class="mb-2 inline-block text-neutral-700 dark:text-neutral-200"
            >NIK</label
            >
            <input
                name="NIK"
                class="relative m-0 block w-full min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-clip-padding py-[0.32rem] px-3 text-base font-normal text-neutral-700 transition duration-300 ease-in-out file:-mx-3 file:-my-[0.32rem] file:overflow-hidden file:rounded-none file:border-0 file:border-solid file:border-inherit file:bg-neutral-100 file:px-3 file:py-[0.32rem] file:text-neutral-700 file:transition file:duration-150 file:ease-in-out file:[margin-inline-end:0.75rem] file:[border-inline-end-width:1px] hover:file:bg-neutral-200 focus:border-sky focus:text-neutral-700 focus:shadow-[0_0_0_1px] focus:shadow-sky focus:outline-none dark:border-neutral-600 dark:text-neutral-200 dark:file:bg-neutral-700 dark:file:text-neutral-100"
                type="text"
                id="NIK"
                value="<?php echo $user->NIK?>" />
        </div>
        <!-- nama -->
        <div class="mb-3 w-96">
            <label
            for="nama"
            class="mb-2 inline-block text-neutral-700 dark:text-neutral-200"
            >Nama</label
            >
            <input
                name="nama"
                class="relative m-0 block w-full min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-clip-padding py-[0.32rem] px-3 text-base font-normal text-neutral-700 transition duration-300 ease-in-out file:-mx-3 file:-my-[0.32rem] file:overflow-hidden file:rounded-none file:border-0 file:border-solid file:border-inherit file:bg-neutral-100 file:px-3 file:py-[0.32rem] file:text-neutral-700 file:transition file:duration-150 file:ease-in-out file:[margin-inline-end:0.75rem] file:[border-inline-end-width:1px] hover:file:bg-neutral-200 focus:border-sky focus:text-neutral-700 focus:shadow-[0_0_0_1px] focus:shadow-sky focus:outline-none dark:border-neutral-600 dark:text-neutral-200 dark:file:bg-neutral-700 dark:file:text-neutral-100"
                type="text"
                id="nama" 
                value="<?php echo $user->nama?>"
                />
        </div>
        <!-- propinsi -->
        <div class="mb-3 w-96">
            <label
            for="propinsi"
            class="mb-2 inline-block text-neutral-700 dark:text-neutral-200"
            >Propinsi</label>
            <select
                name="id_propinsi"
                class="relative m-0 block w-full min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-clip-padding py-[0.32rem] px-3 text-base font-normal text-neutral-700 transition duration-300 ease-in-out file:-mx-3 file:-my-[0.32rem] file:overflow-hidden file:rounded-none file:border-0 file:border-solid file:border-inherit file:bg-neutral-100 file:px-3 file:py-[0.32rem] file:text-neutral-700 file:transition file:duration-150 file:ease-in-out file:[margin-inline-end:0.75rem] file:[border-inline-end-width:1px] hover:file:bg-neutral-200 focus:border-sky focus:text-neutral-700 focus:shadow-[0_0_0_1px] focus:shadow-sky focus:outline-none dark:border-neutral-600 dark:text-neutral-200 dark:file:bg-neutral-700 dark:file:text-neutral-100"
            >
                <?php
                    foreach($ar_prop as $id=>$nama_propinsi){
                        $selected = ($id==$user->id_propinsi)?'selected':'';
                        echo '<option '.$selected.' value="'.$id.'" >'.$nama_propinsi.'</option>';
                    }
                ?>
            </select>
        </div>
        <!-- alamat -->
        <div class="mb-3 w-96">
            <label
            for="alamat"
            class="mb-2 inline-block text-neutral-700 dark:text-neutral-200"
            >Alamat</label
            >
            <textarea
                name="alamat"
                class="relative m-0 block w-full min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-clip-padding py-[0.32rem] px-3 text-base font-normal text-neutral-700 transition duration-300 ease-in-out file:-mx-3 file:-my-[0.32rem] file:overflow-hidden file:rounded-none file:border-0 file:border-solid file:border-inherit file:bg-neutral-100 file:px-3 file:py-[0.32rem] file:text-neutral-700 file:transition file:duration-150 file:ease-in-out file:[margin-inline-end:0.75rem] file:[border-inline-end-width:1px] hover:file:bg-neutral-200 focus:border-sky focus:text-neutral-700 focus:shadow-[0_0_0_1px] focus:shadow-sky focus:outline-none dark:border-neutral-600 dark:text-neutral-200 dark:file:bg-neutral-700 dark:file:text-neutral-100"
                type="text"
                id="alamat"><?php echo $user->alamat?></textarea>
        </div>
        <!-- Tombol simpan -->
        <div class="mb-3 w-96">
            <input type="submit" name="submit" value="SIMPAN" 
            class="inline-block rounded bg-sky-500 px-6 pt-2.5 pb-2 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-sky-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-sky-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-sky-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]" />
        </div>
    </div>
</form>