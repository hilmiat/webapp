<form method="POST">
    <div class="flex justify-center flex-col">
        <!-- NIK -->
        <div class="mb-3 w-96">
            <label
            for="NIK"
            class="mb-2 inline-block text-neutral-700 dark:text-neutral-200"
            >NIK</label
            >
            <input
                oninput="numberOnly(this)"
                name="NIK"
                class="relative m-0 block w-full min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-clip-padding py-[0.32rem] px-3 text-base font-normal text-neutral-700 transition duration-300 ease-in-out file:-mx-3 file:-my-[0.32rem] file:overflow-hidden file:rounded-none file:border-0 file:border-solid file:border-inherit file:bg-neutral-100 file:px-3 file:py-[0.32rem] file:text-neutral-700 file:transition file:duration-150 file:ease-in-out file:[margin-inline-end:0.75rem] file:[border-inline-end-width:1px] hover:file:bg-neutral-200 focus:border-sky focus:text-neutral-700 focus:shadow-[0_0_0_1px] focus:shadow-sky focus:outline-none dark:border-neutral-600 dark:text-neutral-200 dark:file:bg-neutral-700 dark:file:text-neutral-100"
                type="text"
                id="NIK" />
        </div>
        <!-- nama -->
        <div class="mb-3 w-96">
            <label
            for="nama"
            class="mb-2 inline-block text-neutral-700 dark:text-neutral-200"
            >Nama</label
            >
            <input
                oninput="cekInput(this)"
                name="nama"
                class="relative m-0 block w-full min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-clip-padding py-[0.32rem] px-3 text-base font-normal text-neutral-700 transition duration-300 ease-in-out file:-mx-3 file:-my-[0.32rem] file:overflow-hidden file:rounded-none file:border-0 file:border-solid file:border-inherit file:bg-neutral-100 file:px-3 file:py-[0.32rem] file:text-neutral-700 file:transition file:duration-150 file:ease-in-out file:[margin-inline-end:0.75rem] file:[border-inline-end-width:1px] hover:file:bg-neutral-200 focus:border-sky focus:text-neutral-700 focus:shadow-[0_0_0_1px] focus:shadow-sky focus:outline-none dark:border-neutral-600 dark:text-neutral-200 dark:file:bg-neutral-700 dark:file:text-neutral-100"
                type="text"
                id="nama" />
        </div>
        <!-- propinsi -->
        <div class="mb-3 w-96">
            <label
            for="id_propinsi"
            class="mb-2 inline-block text-neutral-700 dark:text-neutral-200"
            >Propinsi</label
            >
            <input
                oninput="cariPropinsi(this)"
                name="propinsi"
                class="relative m-0 block w-full min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-clip-padding py-[0.32rem] px-3 text-base font-normal text-neutral-700 transition duration-300 ease-in-out file:-mx-3 file:-my-[0.32rem] file:overflow-hidden file:rounded-none file:border-0 file:border-solid file:border-inherit file:bg-neutral-100 file:px-3 file:py-[0.32rem] file:text-neutral-700 file:transition file:duration-150 file:ease-in-out file:[margin-inline-end:0.75rem] file:[border-inline-end-width:1px] hover:file:bg-neutral-200 focus:border-sky focus:text-neutral-700 focus:shadow-[0_0_0_1px] focus:shadow-sky focus:outline-none dark:border-neutral-600 dark:text-neutral-200 dark:file:bg-neutral-700 dark:file:text-neutral-100"
                type="text"
                id="propinsi" />

            <input type="hidden" name="id_propinsi" id="id_propinsi" />

            <ul id="pilihan_propinsi">
            </ul>
        </div>


        <!-- alamat -->
        <div class="mb-3 w-96">
            <label
            for="alamat"
            class="mb-2 inline-block text-neutral-700 dark:text-neutral-200"
            >Alamat</label
            >
            <textarea
                oninput="cekInput(this)"
                name="alamat"
                class="relative m-0 block w-full min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-clip-padding py-[0.32rem] px-3 text-base font-normal text-neutral-700 transition duration-300 ease-in-out file:-mx-3 file:-my-[0.32rem] file:overflow-hidden file:rounded-none file:border-0 file:border-solid file:border-inherit file:bg-neutral-100 file:px-3 file:py-[0.32rem] file:text-neutral-700 file:transition file:duration-150 file:ease-in-out file:[margin-inline-end:0.75rem] file:[border-inline-end-width:1px] hover:file:bg-neutral-200 focus:border-sky focus:text-neutral-700 focus:shadow-[0_0_0_1px] focus:shadow-sky focus:outline-none dark:border-neutral-600 dark:text-neutral-200 dark:file:bg-neutral-700 dark:file:text-neutral-100"
                type="text"
                id="alamat"></textarea>
        </div>
        <!-- Tombol simpan -->
        <div class="mb-3 w-96">
            <input disabled="true" type="submit" name="submit" id="submit" value="LENGKAPI DATA" 
            class="inline-block 
            rounded bg-gray-500 px-6 pt-2.5 pb-2 
            text-xs font-medium uppercase 
            leading-normal text-white 
            shadow-[0_4px_9px_-4px_#3b71ca] 
            transition duration-150 
            ease-in-out hover:bg-sky-600 
            hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] 
            focus:bg-sky-600 
            focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] 
            focus:outline-none 
            focus:ring-0 
            active:bg-sky-700 
            active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]" />
        </div>
    </div>
</form>

<script>
    let data_propinsi = ['Jawa Barat','Jakarta','Aceh'];
    function cariPropinsi(el){
        //mendapatkan obj untuk menampilkan pilihan
        let obj_pilihan = document.getElementById("pilihan_propinsi");
        let data_list = '';
        //membaca apa yang diketik
        let diketik = el.value;
        //mencari, dari data_propinsi adakah yang sama dengan yang diketik
        for(propinsi in data_propinsi){
            // jika ada, maka tampilkan dalam list
            if(data_propinsi[propinsi].toUpperCase().indexOf(diketik.toUpperCase()) > -1){
                console.log('ada yang match');
                //buat agar ketika di klik, maka isi dari inputan propinsi adalah data_propinsi[propinsi]
                // juga isi id_propinsi kedalam field hidden id_propinsi
                data_list += '<li><a onclick="setPilihan('+propinsi+')">'+data_propinsi[propinsi]+'</a></li>';
            }
        }
        obj_pilihan.innerHTML = data_list;
    }
    function setPilihan(pilihan){
        //mengisi inputan propinsi dengan pilihan
        document.getElementById('propinsi').value=data_propinsi[pilihan];
        //mengisi inputan id_propinsi dengan id dari pilihan
        document.getElementById('id_propinsi').value=pilihan;
    }

    let inputan = {'NIK':false,'nama':false,'alamat':false};
    function cekInput(el){
        let isi = el.value;
        if(isi == ""){
            inputan[el.name]=false;
        }else{
            inputan[el.name]=true;
        }
        //jika sudah true semua baru di enable, kalau masih ada yang false di disabled dulu
        Object.values(inputan).every(i=>i==true)?enableButton():disableButton();
        // let statusSemuaInputan = Object.values(inputan);
        // if(statusSemuaInputan.every(i=>i==true)){
        //     enableButton();
        // }else{
        //     disableButton();
        // }
    }

    //fungsi untuk menghidupkan tombol
    function enableButton(){
        let tbl = document.getElementById('submit');
        tbl.value = 'SUBMIT'
        tbl.disabled = false;
        tbl.classList.remove('bg-gray-500');
        tbl.classList.add('bg-sky-500');
    }
    //fungsi untuk mematikan tombol
    function disableButton(){
        let tbl = document.getElementById('submit');
        tbl.value = 'LENGKAPI DATA'
        tbl.disabled = true;
        tbl.classList.add('bg-gray-500');
        tbl.classList.remove('bg-sky-500');
    }

    function numberOnly(el){
        //baca value dari input
        let isian = el.value;
        //ganti selain angka dengan ""
        let isianTerfilter = isian.replace(/[^0-9]/g,"");
        //isi value dari input dengan isian yang sudah difilter
        el.value = isianTerfilter;

        //value harus tepat 16
        if(isianTerfilter.length > 16){
            el.value = isianTerfilter.slice(0,16);
        }else if(isianTerfilter.length==16){
            inputan[el.name] = true;
        }else{
            inputan[el.name] = false;
        }

    }

</script>
