<?php
require_once BASE_PATH.'/user/model_user.php';
class UserController{
    private $model;
    public function __construct(){
        // buat obj model user
        $this->model = new ModelUser(); 
    }
    public function action(){
        $act = isset($_GET['act'])?$_GET['act']:'index';
        //routing modul user
        // $act = $_GET['act']??'index';
        switch($act){
            case 'index':
                $this->index();
                break;
            case 'add':
                $this->create();
                break;
            case 'delete':
                $this->delete();
                break;
            case 'update':
                $this->update();
                break;
            default:
                $this->index();
        }
    }
    //action index
    public function index(){
        //baca parameter namaCari
        $paramCari = isset($_GET['paramCari'])?$_GET['paramCari']:"";
        $sortParam = isset($_GET['sortParam'])?$_GET['sortParam']:"";
        //get all data user
        // $users = $this->model->getAllUser();
        // $users = $this->model->findUser(array('field'=>'nama','searchvalue'=>"%$paramCari%"));
        $criteria = array(
            'field'=>array('nama','NIK','alamat'),
            'searchvalue'=>"%$paramCari%",
            // 'sort' => 'nama DESC'
        );
        if($sortParam!=""){
            $criteria['sort'] = $sortParam;
        }
        $page = isset($_GET['page'])?$_GET['page']:1;
        //jumlah data per halaman
        $perpage = 2;
        //offset = page * perpage (1->0;2->2;3->4;4->6)
        $offset = ($page-1) * $perpage;

        $criteria['limit'] = $perpage;
        $criteria['offset'] = $offset;

        $users = $this->model->findUser($criteria);

        //tampilkan data user dlm bentuk tabel
        $this->loadView('view/ui_user',array('users'=>$users,'pesan'=>'helo','paramCari'=>$paramCari),'Tabel User');
    }
    //action delete
    public function delete(){
        //baca id yang mau di delete
        $id_delete = $_GET['id'];
        $status = $this->model->deleteUser($id_delete);
        if($status){
            // header('location:user_controller.php');
            URL_Helper::redirect('user/user_controller','index',null);
        }
    }
    //action add
    public function create(){
        //cek apakah menampilkan form atau proses form
        if(isset($_POST['submit'])){
            //proses data
            //validasi dari sisi server
            if($_POST['NIK']==""||$_POST['nama']==""||$_POST['alamat']==""||$_POST['id_propinsi']==0 ){
                echo 'DATA INVALID';
            }else{
                $user_baru = new User('',$_POST['NIK'],$_POST['nama'],$_POST['id_propinsi'],$_POST['alamat']);
                //panggil fungsi insertUser
                $this->model->insertUser($user_baru);
                // header('location:user_controller.php');
                URL_Helper::redirect('user/user_controller','index',null);
            }
        }else{
            $this->loadView('view/form_user',[],'Add User');
        }
    }
    //action update
    public function update(){
        //baca parameter id user yang akan diupdate
        $id_diupdate = $_GET['id'];
        //get user berdasarkan id
        $user = $this->model->getUserById($id_diupdate);
        if($user==null){
            echo 'user not found';
        }else{
             //cek apakah menampilkan form atau proses form
            if(isset($_POST['submit'])){
                //proses data
                $user_baru = new User('',$_POST['NIK'],$_POST['nama'],$_POST['id_propinsi'],$_POST['alamat']);
                //panggil fungsi updateuser
                $this->model->updateUser($id_diupdate,$user_baru);
                // header('location:user_controller.php');
                URL_Helper::redirect('user/user_controller','index',null);
            }else{
                $this->loadView('view/form_update',array('user'=>$user),'Ubah User');
            }
        }

    }

    //fungsi untuk load tampilan
    private function loadView($file,$data,$halaman){
        foreach($data as $key => $value){
            //membuat variable yang namanya adalah index dari elemen $data
            $$key = $value;
        }
        $namaModule = 'User';
        $linkModule = 'user/user_controller';

        // include '../template/header.php';
        include BASE_PATH.'/user/view/breadcrumb.php';
        include BASE_PATH.'/user/'.$file.'.php';
        // include '../template/footer.php';
    }
}
$userController = new UserController();
$userController->action();
?>