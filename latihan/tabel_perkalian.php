<?php $n=5; 
?>
<h1>Tabel Perkalian</h1>
<table>
    <thead>
        <!-- Judul kolom -->
        <tr style="background-color:#EE00EE">
            <th></th>
            <!-- loop untuk judul kolom -->
            <?php
                for($kol=1;$kol<=$n;$kol++){
            ?>
                <th><?php echo $kol; ?></th>
            <?php
                }
            ?>
        </tr>
    </thead>
    <tbody>
        <!-- Isi tabel -->
        <?php
            for($bar=1;$bar<=$n;$bar++){
                $warna = ($bar%2==0) ? "#FF0000" : "#00FFFF";
        ?>
                <tr style="background-color:<?php echo $warna ?>">
                    <!-- judul baris -->
                    <td style="background-color:#EE00EE"><?php echo $bar?></td>

                    <!-- loop untuk tiap kolom -->
                    <?php
                        for($kol=1;$kol<=$n;$kol++){
                    ?>
                        <td><?php echo $bar*$kol; ?></td>
                    <?php
                        }
                    ?>
                </tr>
        <?php
            }
        ?>
</table>